{
    "_id": "Q3EaaLLx5kDXb5vQ",
    "data": {
        "abilities": {
            "cha": {
                "mod": 1
            },
            "con": {
                "mod": 9
            },
            "dex": {
                "mod": 6
            },
            "int": {
                "mod": -3
            },
            "str": {
                "mod": 10
            },
            "wis": {
                "mod": 6
            }
        },
        "attributes": {
            "ac": {
                "details": "all-around vision",
                "value": 39
            },
            "allSaves": {
                "value": "+1 status to all saves vs. magic"
            },
            "hp": {
                "details": "",
                "max": 275,
                "temp": 0,
                "value": 275
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 34
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [
                    {
                        "type": "Climb",
                        "value": "25 feet"
                    },
                    {
                        "type": "Swim",
                        "value": "50 feet"
                    }
                ],
                "value": "40 feet"
            }
        },
        "details": {
            "alignment": {
                "value": "CE"
            },
            "blurb": "",
            "creatureType": "Aberration",
            "level": {
                "value": 18
            },
            "privateNotes": "",
            "publicNotes": "<p>Although even raving fanatics and doom-saying prophets desperately claim the monstrous shoggoth is nothing more than a drug-induced vision or a thankfully unreal nightmare, the truth is altogether more dire. Shoggoths exist, yet they tend keep to the deepest of ocean trenches or the most remote of caverns and ruins, emerging to spread chaos and destruction in their slimy wakes.</p>\n<p>The first shoggoths were created by an alien species to serve as mindless beasts of burden. Their vast bulk, incredible strength, and amorphous nature made them useful slave labor, and their ability to spontaneously form whatever new eyes, mouths, limbs, and other organs they might need made them incredibly versatile. Eventually, the shoggoths developed enough intelligence to rebel against their masters, and now they lurk, patient but potent, in the lightless deeps.</p>\n<p>A shoggoth has goals and methods unknowable to humanoid beings. They remember their eons of servitude and, compared to their mysterious masters, humans, elves, dwarves and other intelligent beings are mere specks which crawl upon the surface of the world, indistinguishable from animals. When a shoggoth rolls its immense, hideous body over a band of explorers, engulfing them in a gelatinous press of flesh and gnawing teeth, it is not so much evil as uncaring.</p>\n<p>Shoggoths can become the object of worship for humanoid cults dedicated to chaos and entropy. The shoggoth does not respond to this worship, but it can be counted on to consume any hapless victim the cult can capture and sacrifice to it. Rumors of shoggoths that have developed even greater intellects are, one would hope, just that, for the damage a shoggoth capable of reasoning could wreak upon a world is unsettling to say the least.</p>",
            "source": {
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 33
            },
            "reflex": {
                "saveDetail": "",
                "value": 30
            },
            "will": {
                "saveDetail": "",
                "value": 30
            }
        },
        "traits": {
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "blinded",
                    "controlled",
                    "critical-hits",
                    "deafened",
                    "precision",
                    "sleep"
                ]
            },
            "dr": [
                {
                    "label": "Acid",
                    "type": "acid",
                    "value": "20"
                },
                {
                    "label": "Cold",
                    "type": "cold",
                    "value": "20"
                },
                {
                    "label": "Sonic",
                    "type": "sonic",
                    "value": "20"
                }
            ],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "aklo"
                ]
            },
            "rarity": {
                "value": "rare"
            },
            "senses": {
                "value": "darkvision, scent (imprecise) 60 feet, tremorsense (imprecise) 60 feet"
            },
            "size": {
                "value": "huge"
            },
            "traits": {
                "custom": "",
                "value": [
                    "aberration",
                    "amphibious"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "yrfY15KIhcsy07PS",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": [
                        "grab"
                    ]
                },
                "bonus": {
                    "value": 35
                },
                "damageRolls": {
                    "nr179pis1deq9qnhchxd": {
                        "damage": "4d10+18",
                        "damageType": "bludgeoning"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "magical",
                        "reach-30"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Pseudopod",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "6Y0H1qKcOuLLE2PK",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>60 feet. A shoggoth constantly voices syllables and mutterings that mortals were not meant to hear. A creature entering the aura or starting its turn in the aura must succeed at a <span data-pf2-check=\"will\" data-pf2-dc=\"38\" data-pf2-traits=\"auditory,aura,incapacitation,mental\" data-pf2-label=\"Maddening Cacophony DC\" data-pf2-show-dc=\"gm\">Will</span> save or become confused for 1 round ([[/br 2d4 #rounds]]{2d4 rounds} on a critical failure). A creature that successfully saves is temporarily immune for 24 hours.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "auditory",
                        "aura",
                        "incapacitation",
                        "mental"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Maddening Cacophony",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "VpGX01UlrrTeJNLL",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p>2d10+15 bludgeoning, <span data-pf2-check=\"fortitude\" data-pf2-traits=\"damaging-effect\" data-pf2-label=\"Constrict DC\" data-pf2-dc=\"40\" data-pf2-show-dc=\"gm\">basic Fortitude</span></p>\n<hr />\n<p>@Localize[PF2E.NPC.Abilities.Glossary.Constrict]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "constrict",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.g26YiEIfSHCpLocV"
                }
            },
            "img": "systems/pf2e/icons/actions/OneAction.webp",
            "name": "Constrict",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "avV91CD5YGdSAFSf",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A creature that begins its turn inside the shoggoth takes [[/r {9d6}[acid]]]{9d6 acid damage}.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Eat Away",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "GARFMQJWGp5RJLUX",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 2
                },
                "description": {
                    "value": "<p>DC 40, 6d6 acid, Escape DC 40, Rupture 40</p>\n<hr />\n<p>@Localize[PF2E.NPC.Abilities.Glossary.Engulf]</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Engulf",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "oU2jA7jJZJFCuyFY",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p><strong>Requirements</strong> The monster's last action was a success with a Strike that lists Grab in its damage entry, or it has a creature grabbed using this action. <strong>Effect</strong> The monster automatically Grabs the target until the end of the monster's next turn. The creature is grabbed by whichever body part the monster attacked with, and that body part can't be used to Strike creatures until the grab is ended. Using Grab extends the duration of the monster's Grab until the end of its next turn for all creatures grabbed by it. A grabbed creature can use the Escape action to get out of the grab, and the Grab ends for a grabbed creatures if the monster moves away from it.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Grab",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "nKQVbmkk6DdDtmhD",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 36
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 700000,
            "type": "lore"
        },
        {
            "_id": "5bQuYWEULuU1fHyU",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 29
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Intimidation",
            "sort": 800000,
            "type": "lore"
        }
    ],
    "name": "Shoggoth",
    "token": {
        "disposition": -1,
        "height": 3,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Shoggoth",
        "width": 3
    },
    "type": "npc"
}
